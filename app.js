var express    = require('express'),
    app        = express(),
    mongodb = require('./db'),
    bodyParser = require('body-parser'),
    port = process.env.PORT || 3000,
    Globals = require('./globals'),
    log4js = require('log4js');
    
    

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');



app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logResponseBody);


app.use(function(req,res,next){
  
  res.header('Access-Control-Allow-Origin', '*'); // We can access from anywhere
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
});

function logResponseBody(req, res, next) {
    var oldWrite = res.write,
        oldEnd = res.end,
        chunks = [],
        t1 = new Date();

    res.write = function (chunk) {
        chunks.push(chunk);
        oldWrite.apply(res, arguments);
    };

    res.end = function (chunk) {
        if (chunk)
            chunks.push(chunk);

//        var body = Buffer.concat(chunks).toString('utf8');
        var t2 = new Date();
//        logger.trace((t2 - t1) + " : Path: " + req.path + " :Req.body:::: " + JSON.stringify(req.body) + " : ResponseBody:::: "+ body);

        oldEnd.apply(res, arguments);
    };

    next();
};


process.on('SIGINT', function() {
    mongodb.close(function(){
        logger.info('closing db');
        process.exit(0);
    });
});

process.on('uncaughtException', function(err) {
    // handle the error safely
    logger.info(err.stack);
});

mongodb.connect(Globals.MongoHost, Globals.MongoPort, Globals.MongoDB, function(err){
    if(err){
        logger.info("Problem in connecting MongoDB.");
    }else{
        logger.info("Connected to MongoDB.");
        app.listen(port, function () {
            logger.info('API\'s work at http://localhost:' + port + " url.");
        });
    }
});

app.get('/', function(req, res) {
     res.send("welcome vle");
});







app.post('/sendEvent',function(req,res){
    
    console.log(""+JSON.stringify(req.body));

    
    mongodb.findByObject('devicestatus',{"data":req.body.data},function(err,result)
     {

         if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result); 

         }


     } );
   


});

app.post('/setState',function(req,res){

    console.log(""+JSON.stringify(req.body));
      
    
    mongodb.save('devicestatus',req.body, function(err, result)
    {
            if(err)
            {

                res.send({"status":0,"message":"error in insertion of the record"});
            }
            else
            {
               res.json(result); 
            }

            
      }  );
});


app.post('/getEvent',function(req,res){
    
   console.log(""+JSON.stringify(req.body));
   
   





});








